import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:socialization/features/patients/api/patient_repository.dart';
import 'package:socialization/features/patients/models/patient_model.dart';

part 'patient_state.dart';

class PatientCubit extends Cubit<PatientState> {
  final PatientsRepository repository;
  final String token;
  final String patientId;
  PatientCubit(
      {@required this.repository,
      @required this.token,
      @required this.patientId})
      : super(PatientInitial());

  void getPatientInfo() async {
    emit(PatientLoading());
    await repository
        .getPatientInfo(token: token, patientId: patientId)
        .then((patient) {
      emit(PatientLoaded(patient: patient));
    });
  }
}
