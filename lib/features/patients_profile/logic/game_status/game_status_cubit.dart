import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:socialization/features/games/api/games_repository.dart';
part 'game_status_state.dart';

class GameStatusCubit extends Cubit<GameStatusState> {
  final GamesRepository repository;
  final String token;
  final String patientId;
  final String gameId;
  final bool gameStatusDetached;
  //false - удаление игры true - доавбление
  GameStatusCubit(
      {@required this.repository,
      @required this.token,
      @required this.gameId,
      @required this.patientId,
      this.gameStatusDetached = true})
      : super(gameStatusDetached ? GameStatusDetached() : GameStatusAttached());

  void attachGame() async {
    emit(GameStatusLoading());
    await repository
        .postGameAttaching(gameId: gameId, patientId: patientId, token: token)
        .then((status) {
      status ? emit(GameStatusAttached()) : emit(GameStatusDetached());
    });
  }

  void detachGame() async {
    emit(GameStatusLoading());
    await repository
        .deleteGameDetaching(gameId: gameId, patientId: patientId, token: token)
        .then((status) {
      status ? emit(GameStatusDetached()) : emit(GameStatusAttached());
    });
  }
}
