part of 'game_status_cubit.dart';

abstract class GameStatusState extends Equatable {
  const GameStatusState();

  @override
  List<Object> get props => [];
}

class GameStatusLoading extends GameStatusState {}

class GameStatusAttached extends GameStatusState {}

class GameStatusDetached extends GameStatusState {}
