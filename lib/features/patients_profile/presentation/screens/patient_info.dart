import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/games/api/games_repository.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/games/presentation/screens/games_screen.dart';
import 'package:socialization/features/patients/models/patient_model.dart';
import 'package:socialization/features/patients/presentation/widgets/status_button.dart';
import 'package:socialization/features/patients_profile/presentation/screens/patient_tab_games.dart';
import 'package:socialization/features/patients_profile/presentation/screens/patient_tab_profile.dart';
import 'package:socialization/features/patients_profile/presentation/widgets/patient_info_buttons.dart';

import '../../logic/cubit/patient_cubit.dart';

class PatientInfoScreen extends StatefulWidget {
  final Patient patient;

  const PatientInfoScreen({
    Key key,
    @required this.patient,
  }) : super(key: key);

  @override
  State<PatientInfoScreen> createState() => _PatientInfoState();
}

class _PatientInfoState extends State<PatientInfoScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildAvatar(widget.patient.image),
              PatientInfoButtons()
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        TabBar(
          controller: _tabController,
          labelColor: Colors.black,
          tabs: [
            Tab(
              text: 'Профиль',
            ),
            Tab(
              text: 'Игры',
            )
          ],
        ),
        Expanded(
          // height: 300,
          child: TabBarView(controller: _tabController, children: [
            Center(
                child: PatientTabProfile(
              patient: widget.patient,
            )),
            Center(child: PatientTabGames()),
          ]),
        )
      ],
    );
  }

  //Отображение фото профиля
  Widget _buildAvatar(String image) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: Container(
        height: 200,
        child: AspectRatio(
          aspectRatio: 3 / 4,
          child: image != null
              ? Image.network(
                  image,
                  fit: BoxFit.cover,
                )
              : Image.asset(
                  'assets/profile_avatar.jpg',
                  fit: BoxFit.cover,
                ),
        ),
      ),
    );
  }
}
