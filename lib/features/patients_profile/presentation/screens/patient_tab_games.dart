//Статистика
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/games/api/games_repository.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/games/presentation/widgets/games_listing.dart';
import '../../logic/cubit/patient_cubit.dart';

class PatientTabGames extends StatelessWidget {
  const PatientTabGames({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Game game = Game(name: "Тетс", image: null);
    // List<int> games = [1, 2, 3, 4, 5, 6, 7, 8];
    return Padding(
      padding: EdgeInsets.all(20),
      child: BlocProvider<GamesCubit>(
        create: (context) => GamesCubit(
            repository: context.read<GamesRepository>(),
            profile: context.read<UserRepository>(),
            patientId: context.read<PatientCubit>().patientId,
            getAllAvailable: false),
        child: GamesListing(),
      ),
    );

    // return SingleChildScrollView(
    //   physics: BouncingScrollPhysics(),
    //   child: Container(
    //     padding: EdgeInsets.symmetric(horizontal: 20),
    //     color: Colors.red,
    //     child: Column(
    //       mainAxisAlignment: MainAxisAlignment.start,
    //       children: [
    //         Text(
    //           'Последние добавленные игры и тесты',
    //           style: TextStyle(
    //               fontSize: 23,
    //               fontWeight: FontWeight.bold,
    //               color: Colors.black),
    //         ),
    //         SizedBox(
    //           height: 20,
    //         ),
    //         GridView.builder(
    //             shrinkWrap: true,
    //             gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
    //                 crossAxisCount: 2, crossAxisSpacing: 30),
    //             physics: NeverScrollableScrollPhysics(),
    //             itemBuilder: (context, index) {
    //               return GameCard(
    //                 game: game,
    //               );
    //             },
    //             itemCount: games.length),
    //       ],
    //     ),
    //   ),
    // );
  }
}
