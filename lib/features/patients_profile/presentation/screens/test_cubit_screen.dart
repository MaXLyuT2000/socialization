import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../logic/cubit/patient_cubit.dart';

class TestCubitScreen extends StatelessWidget {
  const TestCubitScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final patientId = context.read<PatientCubit>().patientId;
    return Scaffold(
      body: Container(child: Text(patientId)),
    );
  }
}
