//Профиль
import 'package:flutter/material.dart';
import 'package:socialization/features/patients/models/patient_model.dart';

class PatientTabProfile extends StatelessWidget {
  final Patient patient;
  const PatientTabProfile({Key key, @required this.patient}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      padding: EdgeInsets.only(left: width * 0.2, right: width * 0.2, top: 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _buildName(patient: patient),
          SizedBox(
            height: 20,
          ),
          _buildInfo(
              icon: Icons.phone,
              infoName: 'Телефон',
              text: patient.numberPhone),
          _buildInfo(icon: Icons.mail, infoName: 'Почта', text: patient.email),
          _buildInfo(
              icon: Icons.date_range,
              infoName: 'Дата рождения',
              text: patient.birthDate),
          _buildInfo(icon: Icons.person, infoName: 'Пол', text: patient.gender),
          patient.tutor != null
              ? _buildInfo(
                  icon: Icons.person,
                  infoName: 'Тьютор',
                  text:
                      '${patient.tutor.name + " " + patient.tutor.surname + " " + patient.tutor.secondName}')
              : _buildInfo(
                  icon: Icons.person, infoName: 'Тьютор', text: 'Неизвестно')
        ],
      ),
    );
  }

  Widget _buildInfo(
      {@required IconData icon,
      @required String infoName,
      @required String text}) {
    final textStyle = TextStyle(
      fontSize: 16,
    );
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(icon),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: Text(
              infoName + ": " + text,
              style: textStyle,
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildName({@required patient}) {
    final textStyle = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
    return Text(
      patient.name + " " + patient.surname + " " + patient.secondName,
      style: textStyle,
      textAlign: TextAlign.start,
    );
  }
}
