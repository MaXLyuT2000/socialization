import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/patients/api/patient_repository.dart';
import 'package:socialization/features/patients/models/patient_model.dart';
import 'package:socialization/features/patients_profile/presentation/screens/patient_info.dart';
import '../../logic/cubit/patient_cubit.dart';

class PatientInfoScreenCubit extends StatelessWidget {
  final Patient patient;

  const PatientInfoScreenCubit({Key key, @required this.patient})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => PatientCubit(
            repository: context.read<PatientsRepository>(),
            token: context.read<UserRepository>().token,
            patientId: patient.id),
        child: Scaffold(
          appBar: AppBar(),
          body: BlocBuilder<PatientCubit, PatientState>(
            builder: (context, state) {
              if (state is PatientLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is PatientLoaded) {
                return PatientInfoScreen(
                  patient: state.patient,
                );
              }
              if (state is PatientInitial) {
                context.read<PatientCubit>().getPatientInfo();
                return Center(child: CircularProgressIndicator());
              }
              return Text('Error');
            },
          ),
        ));
  }
}
