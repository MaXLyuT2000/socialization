import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/patients_profile/logic/cubit/patient_cubit.dart';
import '../../logic/game_status/game_status_cubit.dart';

class GameStatusButton extends StatelessWidget {
  final ButtonStyle buttonStyle;
  final String gameId;
  const GameStatusButton({
    Key key,
    this.buttonStyle,
    this.gameId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GameStatusCubit, GameStatusState>(
      listener: (context, state) {
        if (state is GameStatusAttached) {
          BlocProvider.of<PatientCubit>(context).getPatientInfo();
          BlocProvider.of<GamesCubit>(context).refreshData();
        }
        if (state is GameStatusDetached) {
          BlocProvider.of<PatientCubit>(context).getPatientInfo();
          BlocProvider.of<GamesCubit>(context).refreshData();
        }
      },
      builder: (context, state) {
        return BlocBuilder<GameStatusCubit, GameStatusState>(
          builder: (context, state) {
            if (state is GameStatusDetached) {
              return ElevatedButton(
                style: buttonStyle,
                onPressed: () {
                  BlocProvider.of<GameStatusCubit>(context).attachGame();
                },
                child: Text('Назначить игру'),
              );
            }

            if (state is GameStatusAttached) {
              return ElevatedButton(
                style: buttonStyle,
                onPressed: () {
                  BlocProvider.of<GameStatusCubit>(context).detachGame();
                },
                child: Text('Удалить игру'),
              );
            }

            if (state is GameStatusLoading) {
              return ElevatedButton(
                style: buttonStyle,
                onPressed: () {},
                child: CircularProgressIndicator(),
              );
            }

            return Text('data');
          },
        );
      },
    );
  }
}
