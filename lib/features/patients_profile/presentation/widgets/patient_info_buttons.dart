import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/games/api/games_repository.dart';
import 'package:socialization/features/games/presentation/screens/games_screen.dart';
import 'package:socialization/features/patients/presentation/patient_status/patient_status_cubit.dart';
import 'package:socialization/features/patients/presentation/widgets/status_button.dart';
import '../../../games/presentation/games/games_cubit.dart';
import '../../logic/cubit/patient_cubit.dart';

class PatientInfoButtons extends StatelessWidget {
  const PatientInfoButtons({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final buttonStyle = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(150, 50)),
    );

    return Column(
      children: [
        _buildChangeTraectorybutton(context),
        SizedBox(
          height: 10,
        ),
        StatusButton(
          patientId: context.read<PatientCubit>().patientId,
          buttonStyle: buttonStyle,
        ),
      ],
    );
  }

  Widget _buildChangeTraectorybutton(BuildContext context) {
    final newGamesCubit = GamesCubit(
        repository: context.read<GamesRepository>(),
        profile: context.read<UserRepository>(),
        patientId: context.read<PatientCubit>().patientId,
        getAllAvailable: true);
    final buttonStyle = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(150, 50)),
    );
    final textStyle = TextStyle(
      fontSize: 14,
    );

    return BlocBuilder<PatientStatusCubit, PatientStatusState>(
        builder: (context, state) {
      if (state is PatientStatusAttached) {
        return ElevatedButton(
          style: buttonStyle,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return MultiBlocProvider(
                providers: [
                  BlocProvider<GamesCubit>.value(
                    value: newGamesCubit,
                  ),
                  BlocProvider<PatientCubit>.value(
                    value: context.read<PatientCubit>(),
                  ),
                ],
                child: GamesScreen(),
              );
            }));
          },
          child: Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(text: 'Добавить\n', style: textStyle),
                TextSpan(text: 'игру', style: textStyle),
              ],
            ),
          ),
        );
      }
      return Text('');
    });
  }
}
