import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/profile/presentation/cubit/profile_avatar_dart_cubit.dart';

class ProfileAtMenu extends StatelessWidget {
  const ProfileAtMenu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileAvatarDartCubit, ProfileAvatarDartState>(
        builder: (context, state) {
      if (state is ProfileAvatarInitial) {
        return GestureDetector(
          child: CircleAvatar(
            backgroundImage: state.image != null
                ? NetworkImage(state.image)
                : AssetImage('assets/profile_avatar.jpg'),
          ),
        );
      }

      if (state is ProfileAvatarUploading) {
        return CircleAvatar(backgroundImage: FileImage(state.file));
      }
      return Text('data');
    });
  }
}
