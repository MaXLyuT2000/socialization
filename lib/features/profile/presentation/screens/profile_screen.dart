import 'package:flutter/material.dart';
import 'package:socialization/features/profile/presentation/widgets/buttons.dart';
import 'package:socialization/features/profile/presentation/widgets/profile_avatar.dart';
import 'package:socialization/features/profile/presentation/widgets/profile_info.dart';

class ProfileInfoScreen extends StatelessWidget {
  const ProfileInfoScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 24,
          ),
          ProfileAvatar(),
          const SizedBox(
            height: 24,
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Column(
                  children: [
                    Expanded(child: ProfileInfo()),
                    const SizedBox(
                      height: 24,
                    ),
                    FunctionalButtons(),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                )),
          ),
        ],
      ),
    ));
  }
}
