part of 'profile_avatar_dart_cubit.dart';

abstract class ProfileAvatarDartState extends Equatable {
  const ProfileAvatarDartState();

  @override
  List<Object> get props => [];
}

class ProfileAvatarInitial extends ProfileAvatarDartState {
  final String image;

  ProfileAvatarInitial({@required this.image});
}

class ProfileAvatarUploading extends ProfileAvatarDartState {
  final File file;

  ProfileAvatarUploading({@required this.file});
}
