import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';

part 'profile_avatar_dart_state.dart';

class ProfileAvatarDartCubit extends Cubit<ProfileAvatarDartState> {
  final String image;
  final UserRepository repository;
  ProfileAvatarDartCubit({@required this.repository, @required this.image})
      : super(ProfileAvatarInitial(image: image));

  void uploadImage({@required File file}) async {
    emit(ProfileAvatarUploading(file: file));
    //upload
    await repository.postProfileImage(file).then((newImage) => {
          emit(ProfileAvatarInitial(image: newImage)),
        });
  }
}
