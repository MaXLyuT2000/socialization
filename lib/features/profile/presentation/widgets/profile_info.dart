import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';

class ProfileInfo extends StatelessWidget {
  const ProfileInfo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final profile = context.read<UserRepository>().profile;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _buildName(profile: profile),
        SizedBox(
          height: 20,
        ),
        _buildInfo(infoName: 'Телефон', text: profile.numberPhone),
        _buildInfo(infoName: 'Эл. почта', text: profile.email),
        _buildInfo(infoName: 'Организация', text: profile.organization)
      ],
    );
  }

  Widget _buildInfo({@required String infoName, @required String text}) {
    final textStyle = TextStyle(
      fontSize: 15,
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          infoName + ":",
          style: textStyle,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          text,
          style: textStyle,
        )
      ],
    );
  }

  Widget _buildName({@required profile}) {
    final textStyle = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
    final String profileName = profile.name == null
        ? 'Отсутствует ФИО'
        : profile.name + " " + profile.surname + " " + profile.secondName;
    return Text(
      profileName,
      style: textStyle,
      textAlign: TextAlign.center,
    );
  }
}
