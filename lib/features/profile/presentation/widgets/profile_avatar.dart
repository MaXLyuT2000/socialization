import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:socialization/features/profile/presentation/cubit/profile_avatar_dart_cubit.dart';

class ProfileAvatar extends StatefulWidget {
  const ProfileAvatar({
    Key key,
  }) : super(key: key);

  @override
  State<ProfileAvatar> createState() => _ProfileAvatarState();
}

class _ProfileAvatarState extends State<ProfileAvatar> {
  File _pickedFile;
  CroppedFile _croppedFile;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(child:
        BlocBuilder<ProfileAvatarDartCubit, ProfileAvatarDartState>(
            builder: (context, state) {
      if (state is ProfileAvatarInitial) {
        return Stack(
          children: [
            buildImage(context, state.image),
            Positioned(bottom: 0, right: 4, child: buildEditIcon(context))
          ],
        );
      }
      if (state is ProfileAvatarUploading) {
        return buildFile(context, state.file);
      }
      return Text('data');
    }));
  }

  Future<void> changePhoto() async {
    await showImageSourse(context).then((source) async {
      if (source == null) {
        return;
      } else {
        await _pickImage(source).then((status) async {
          if (status == null) {
            return;
          } else {
            await _cropImage().then((status) async {
              if (status == null) {
                return;
              } else {
                BlocProvider.of<ProfileAvatarDartCubit>(context)
                    .uploadImage(file: File(_pickedFile.path));
              }
            });
          }
        });
      }
    });
  }

  //Отображение фото профиля (initial)
  Widget buildImage(BuildContext context, String image) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: InkWell(
          onTap: () {
            changePhoto();
          },
          child: Container(
            height: 200,
            child: AspectRatio(
              aspectRatio: 3 / 4,
              child: image != null
                  ? Image.network(
                      image,
                      fit: BoxFit.cover,
                    )
                  : Image.asset(
                      'assets/profile_avatar.jpg',
                      fit: BoxFit.cover,
                    ),
            ),
          )),
    );
  }

  //Отображение локального фото (при upload)
  Widget buildFile(BuildContext context, File file) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: Container(
        height: 200,
        child: Stack(
          children: [
            AspectRatio(
              aspectRatio: 3 / 4,
              child: Image.file(
                File(_pickedFile.path),
                fit: BoxFit.cover,
              ),
            ),
            Positioned.fill(
                child: Align(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator())),
          ],
        ),
      ),
    );
  }

  Widget buildEditIcon(BuildContext context) {
    return ClipOval(
      child: Container(
        color: Theme.of(context).primaryColor,
        padding: EdgeInsets.all(8),
        child: Icon(
          Icons.edit,
          color: Colors.white,
          size: 25,
        ),
      ),
    );
  }

  //Выбор откуда берем изображение с popUp (Amdroid, IOS)
  Future<ImageSource> showImageSourse(BuildContext context) async {
    if (Platform.isIOS) {
      return showCupertinoModalPopup<ImageSource>(
          context: context,
          builder: (context) => CupertinoActionSheet(
                actions: [
                  CupertinoActionSheetAction(
                    child: Text('Камера'),
                    onPressed: () =>
                        Navigator.of(context).pop(ImageSource.camera),
                  ),
                  CupertinoActionSheetAction(
                    child: Text('Галерея'),
                    onPressed: () =>
                        Navigator.of(context).pop(ImageSource.gallery),
                  ),
                ],
              ));
    } else {
      return showModalBottomSheet(
          context: context,
          builder: (context) => Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    leading: Icon(Icons.camera_alt),
                    title: Text('Камера'),
                    onTap: () => Navigator.of(context).pop(ImageSource.camera),
                  ),
                  ListTile(
                    leading: Icon(Icons.image),
                    title: Text('Галерея'),
                    onTap: () => Navigator.of(context).pop(ImageSource.gallery),
                  )
                ],
              ));
    }
  }

  //Выбор пути к файлу по source (Фото, Галерея)
  Future<File> _pickImage(ImageSource source) async {
    try {
      final pickedFile = await ImagePicker().pickImage(source: source);
      if (pickedFile != null) {
        setState(() {
          _pickedFile = File(pickedFile.path);
        });
        return _pickedFile;
      } else {
        return null;
      }
    } catch (e) {
      print('Failed to pick image $e');
      return null;
    }
  }

  //Обрезать изображение
  Future<bool> _cropImage() async {
    try {
      if (_pickedFile != null) {
        final croppedFile = await ImageCropper().cropImage(
            sourcePath: _pickedFile.path,
            compressFormat: ImageCompressFormat.jpg,
            compressQuality: 100,
            uiSettings: buildUiSettings(context));
        if (croppedFile != null) {
          setState(() {
            _pickedFile = File(croppedFile.path);
          });
          return true;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  List<PlatformUiSettings> buildUiSettings(BuildContext context) {
    return [
      AndroidUiSettings(
          toolbarTitle: 'Обрезать фото',
          // toolbarColor: Colors.deepOrange,
          // toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
      IOSUiSettings(
        title: 'Обрезать фото',
      ),
    ];
  }
}
