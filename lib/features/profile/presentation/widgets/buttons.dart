import 'package:flutter/material.dart';
import 'package:socialization/features/authorization/presentation/widgets/allert_dialog.dart';

class FunctionalButtons extends StatelessWidget {
  const FunctionalButtons({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final textStyle = TextStyle(fontSize: 18);
    final buttonStyle = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(width - 100, 50)),
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // ElevatedButton(
        //   onPressed: () {
        //     changePhotoCB();
        //   },
        //   style: buttonStyle,
        //   child: Text('Сменить фото', style: textStyle),
        // ),
        // SizedBox(
        //   height: 12,
        // ),
        // ThemeSwitcher(
        //   builder: (context) {
        //     return ElevatedButton(
        //       onPressed: () async {
        //         {
        //           var themeName = ThemeModelInheritedNotifier.of(context)
        //                       .theme
        //                       .brightness ==
        //                   Brightness.light
        //               ? 'dark'
        //               : 'light';
        //           var service = await ThemeService.instance
        //             ..save(themeName);
        //           var theme = service.getByName(themeName);
        //           ThemeSwitcher.of(context).changeTheme(theme: theme);
        //         }
        //       },
        //       style: buttonStyle,
        //       child: Text(
        //         'Темная тема',
        //         style: textStyle,
        //       ),
        //     );
        //   },
        // ),
        // SizedBox(
        //   height: 12,
        // ),
        ElevatedButton(
          onPressed: () {
            showAlertErrorDialog(context);
          },
          style: buttonStyle,
          child: Text(
            'Выход',
            style: textStyle,
          ),
        )
      ],
    );
  }
}
