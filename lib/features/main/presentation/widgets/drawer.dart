import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/common/app_styles.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/authorization/models/profile.dart';
import 'package:socialization/features/authorization/presentation/widgets/allert_dialog.dart';
import 'package:socialization/features/profile/presentation/cubit/profile_avatar_dart_cubit.dart';
import 'package:socialization/features/profile/presentation/screens/profile_screen.dart';

class MenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final reposetory = context.read<UserRepository>();
    final User profile = context.read<UserRepository>().profile;
    final ProfileAvatarDartCubit cubit =
        ProfileAvatarDartCubit(repository: reposetory, image: profile.image);

    return BlocProvider<ProfileAvatarDartCubit>(
      create: (context) => cubit,
      child: Drawer(
        child: Container(
          child: ListView(
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              BlocBuilder<ProfileAvatarDartCubit, ProfileAvatarDartState>(
                  builder: (context, state) {
                if (state is ProfileAvatarInitial) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => BlocProvider.value(
                                    value: cubit,
                                    child: ProfileInfoScreen(),
                                  )));
                      // Navigator.pushNamed(context, '/ProfileInfo');
                    },
                    child: UserAccountsDrawerHeader(
                      decoration:
                          BoxDecoration(color: Theme.of(context).primaryColor),
                      accountName: Text(profile.name +
                          " " +
                          profile.surname +
                          " " +
                          profile.secondName),
                      accountEmail: Text(profile.email),
                      currentAccountPicture: CircleAvatar(
                        backgroundImage: profile.image != null
                            ? NetworkImage(profile.image)
                            : AssetImage('assets/profile_avatar.jpg'),
                      ),
                    ),
                  );
                }

                if (state is ProfileAvatarUploading) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => BlocProvider.value(
                                    value: cubit,
                                    child: ProfileInfoScreen(),
                                  )));
                      // Navigator.pushNamed(context, '/ProfileInfo');
                    },
                    child: UserAccountsDrawerHeader(
                      decoration: BoxDecoration(color: AppStyles.mainColor),
                      accountName: Text(profile.name +
                          " " +
                          profile.surname +
                          " " +
                          profile.secondName),
                      accountEmail: Text(profile.email),
                      currentAccountPicture: CircleAvatar(
                        backgroundImage: profile.image != null
                            ? NetworkImage(profile.image)
                            : AssetImage('assets/profile_avatar.jpg'),
                      ),
                    ),
                  );
                }
                return Text('data');
              }),
              context.read<UserRepository>().role == 3
                  ? ListTile(
                      title: new Text("Наблюдаемые"),
                      leading: new Icon(Icons.face),
                      onTap: () {
                        Navigator.pushNamed(context, '/Observers');
                      },
                    )
                  : SizedBox(),
              ListTile(
                title: new Text("Пункт меню"),
                leading: new Icon(Icons.help),
                onTap: () {},
              ),
              ListTile(
                  title: new Text("Выход"),
                  leading: new Icon(Icons.exit_to_app),
                  onTap: () {
                    showAlertErrorDialog(context);
                  })
            ],
          ),
        ),
      ),
    );
  }

  Widget buildImage(BuildContext context) {}
}
