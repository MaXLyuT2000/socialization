import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/games/presentation/screens/games_screen.dart';
import 'package:socialization/features/patients/presentation/screens/patients_list/patients_screen.dart';
import 'package:socialization/features/profile/presentation/screens/profile_at_menu.dart';
import 'package:socialization/features/profile/presentation/screens/profile_screen.dart';
import 'package:socialization/screens/empty_screen.dart';

class BotNavBar extends StatefulWidget {
  @override
  _BotNavBarState createState() => _BotNavBarState();
}

class _BotNavBarState extends State<BotNavBar> {
  Roles role;
  int _currentIndex = 1;
  List<StatelessWidget> pages;
  List<BottomNavigationBarItem> items;

  @override
  void initState() {
    super.initState();
    role = context.read<UserRepository>().role;
    switch (role) {
      case Roles.Tutor:
        pages = [
          ProfileInfoScreen(),
          EmptyScreen(),
          EmptyScreen(),
          PatientsScreen(),
        ];
        items = [
          BottomNavigationBarItem(
              icon: ProfileAtMenu(),
              label: 'Профиль',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.graphic_eq_outlined),
              label: 'Статистика',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined),
              label: 'Сообщения',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_outline),
              label: 'Пациенты',
              backgroundColor: Colors.blue),
        ];
        _currentIndex = 3;
        break;
      case Roles.Patient:
        pages = [
          ProfileInfoScreen(),
          GamesScreen(),
          EmptyScreen(),
        ];
        items = [
          BottomNavigationBarItem(
              icon: ProfileAtMenu(),
              label: 'Профиль',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.graphic_eq_outlined),
              label: 'Игры',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined),
              label: 'Сообщения',
              backgroundColor: Colors.blue),
        ];
        _currentIndex = 1;
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: pages,
      ),

      // pages.elementAt(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        // fixedColor: styleBiege,
        currentIndex: _currentIndex,
        iconSize: 30,
        type: BottomNavigationBarType.fixed,
        items: items,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
