import 'package:equatable/equatable.dart';

class Game extends Equatable {
  final String id;
  final String name;
  final String description;
  final String link;
  final String image;
  final String assignedBy;
  final gamePatientId;
  final String active;
  final String assignmentDate;
  final String type;
  final String status;
  final bool useStatistics;

  Game(
      {this.id,
      this.name,
      this.description,
      this.link,
      this.image,
      this.assignedBy,
      this.gamePatientId,
      this.active,
      this.assignmentDate,
      this.type,
      this.status,
      this.useStatistics});

  factory Game.fromJson(Map<String, dynamic> json) {
    return Game(
        id: json['id'],
        name: json['name'] ?? 'Неизвестно',
        description: json['description'],
        link: json['url'],
        useStatistics: json['useStatistic'],
        image: json['image'] == null
            ? null
            : 'https://mobile.itkostroma.ru/images/' + json['image'],
        assignedBy: json['assignedBy'],
        gamePatientId: json['gamePatientId'],
        active: json['active'],
        assignmentDate: json['assignmentDate'],
        type: json['type'],
        status: json['status']);
  }

  @override
  // TODO: implement props
  List<Object> get props => [id, name, description, link];
}

//Модель пагинации
class GamesPage {
  final bool next;
  final List<Game> games;

  GamesPage({this.next, this.games});
}
