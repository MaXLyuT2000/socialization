import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/games/api/games_repository.dart';
import 'package:socialization/features/games/models/game_model.dart';
import 'package:socialization/features/patients/models/filter_model.dart';

part 'games_state.dart';

class GamesCubit extends Cubit<GamesState> {
  final GamesRepository repository;
  final UserRepository profile;
  final String patientId;
  final bool getAllAvailable;

  final List<FilterCategory> filters = [
    FilterCategory(idCategory: '1', nameCategory: 'По названию'),
    FilterCategory(idCategory: '2', nameCategory: 'По дате'),
    FilterCategory(idCategory: '3', nameCategory: 'По типу'),
  ];

  FilterCategory selectedFilterCategory =
      FilterCategory(idCategory: '1', nameCategory: 'По названию');

  String patientSearchRegex = '';
  GamesCubit({
    @required this.repository,
    @required this.profile,
    this.patientId = '',
    this.getAllAvailable = false,
  }) : super(GamesInitial());

  int page = 0;
  bool allLoaded = false;

  void loadPosts() {
    if ((state is GamesLoading) || (allLoaded)) return;

    final currentState = state;

    var oldGames = <Game>[];
    if (currentState is GamesLoaded) {
      oldGames = currentState.games;
    }

    emit(GamesLoading(oldGames, isFirstFetch: page == 0));

    if (profile.role == Roles.Patient) {
      repository
          .getAllGames(
              regex: patientSearchRegex,
              page: page,
              token: profile.token,
              role: profile.role,
              category: selectedFilterCategory)
          .then((newPage) {
        if (newPage.next == false) {
          allLoaded = true;
        }
        page++;
        var sortByStatusList =
            newPage.games.where((game) => game.status == "ASSIGNED").toList();
        oldGames.addAll(sortByStatusList);

        emit(GamesLoaded(oldGames));
      });
    } else {
      repository
          .getTutorPatientGames(
              regex: patientSearchRegex,
              page: page,
              token: profile.token,
              patientId: patientId,
              getAllAvailable: getAllAvailable)
          .then((newPage) {
        if (newPage.next == false) {
          allLoaded = true;
        }
        page++;
        if (getAllAvailable) {
          oldGames.addAll(newPage.games);
        } else {
          var sortByStatusList =
              newPage.games.where((game) => game.active == "ACTIVE").toList();
          oldGames.addAll(sortByStatusList);
        }

        emit(GamesLoaded(oldGames));
      });
    }
  }

  void searchGames(String regex) async {
    page = 0;
    patientSearchRegex = regex;
    allLoaded = false;

    emit(GamesInitial());
    loadPosts();
  }

  //Поиск по фильтру
  void sortByFilter(FilterCategory filterCategory) {
    selectedFilterCategory = filterCategory;
    refreshData();
  }

  void refreshData() {
    final currentState = state;
    if (currentState is GamesLoaded) {
      currentState.games = [];
    }
    page = 0;
    allLoaded = false;
    loadPosts();
  }
}
