part of 'games_cubit.dart';

abstract class GamesState extends Equatable {
  const GamesState();

  @override
  List<Object> get props => [];
}

class GamesInitial extends GamesState {}

class GamesLoaded extends GamesState {
  List<Game> games;

  GamesLoaded(this.games);
}

class GamesLoading extends GamesState {
  final List<Game> oldGames;
  final bool isFirstFetch;

  GamesLoading(this.oldGames, {this.isFirstFetch = false});
}
