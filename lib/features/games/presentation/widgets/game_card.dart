import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/games/models/game_model.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/games/presentation/screens/game_info.dart';
import 'package:socialization/features/patients_profile/logic/cubit/patient_cubit.dart';

class GameCard extends StatelessWidget {
  final Game game;

  GameCard({Key key, @required this.game}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final role = context.read<UserRepository>().role;
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (_) {
          //Разделение по ролям
          if (role == Roles.Tutor) {
            return MultiBlocProvider(
              providers: [
                BlocProvider.value(
                  value: context.read<PatientCubit>(),
                ),
                BlocProvider.value(
                  value: context.read<GamesCubit>(),
                )
              ],
              child: GameInfo(
                game: game,
              ),
            );
          } else {
            return GameInfo(
              game: game,
            );
          }
        }));
      },
      child: Column(
        children: [
          _gameImage(game.image),
          SizedBox(
            height: 10,
          ),
          _buildName(game),
        ],
      ),
    );
  }

  Widget _gameImage(String image) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: image != null
          ? Image.network(
              image,
              fit: BoxFit.cover,
            )
          : Image.asset(
              'assets/empty_game.jpg',
              fit: BoxFit.cover,
            ),
    );
  }

  Widget _buildName(Game game) {
    return Expanded(
      child: Text(
        game.name,
        maxLines: 2,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
