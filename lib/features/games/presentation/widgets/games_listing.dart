import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/games/models/game_model.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/games/presentation/widgets/game_card.dart';

class GamesListing extends StatelessWidget {
  final scrollController = ScrollController();

  GamesListing({Key key}) : super(key: key);

  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<GamesCubit>(context).loadPosts();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    // BlocProvider.of<PatientsCubit>(context).loadPosts();
    return BlocBuilder<GamesCubit, GamesState>(builder: (context, state) {
      Future _refreshData() async {
        BlocProvider.of<GamesCubit>(context).refreshData();
      }

      if (state is GamesInitial) {
        _refreshData();
      }

      if (state is GamesLoading && state.isFirstFetch) {
        return _loadingIndicator();
      }

      List<Game> posts = [];
      bool isLoading = false;

      if (state is GamesLoading) {
        posts = state.oldGames;
        isLoading = true;
      } else if (state is GamesLoaded) {
        posts = state.games;
      }

      return RefreshIndicator(
        onRefresh: _refreshData,
        child: posts.isEmpty
            ? _emptyList()
            : GridView.builder(
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 2 / 3.5,
                    crossAxisCount: 2,
                    crossAxisSpacing: 30),
                physics: const AlwaysScrollableScrollPhysics(),
                controller: scrollController,
                itemBuilder: (context, index) {
                  if (index < posts.length) {
                    return GameCard(
                      game: posts[index],
                      // patient: posts[index],
                      // addPatient: true,
                    );
                  } else {
                    Timer(const Duration(milliseconds: 30), () {
                      scrollController
                          .jumpTo(scrollController.position.maxScrollExtent);
                    });

                    return _loadingIndicator();
                  }
                },
                itemCount: posts.length + (isLoading ? 1 : 0),
              ),
      );
    });
  }

  Widget _emptyList() {
    return Stack(
      children: [
        ListView(),
        Center(child: Text('Список игр пуст')),
      ],
    );
  }

  Widget _loadingIndicator() {
    return const Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }
}
