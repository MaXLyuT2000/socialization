import 'package:flutter/material.dart';
import 'package:socialization/features/games/presentation/widgets/games_listing.dart';
import 'package:socialization/features/patients/presentation/widgets/search_bar.dart';

class GamesScreen extends StatelessWidget {
  const GamesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _pageTitle(),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              SearchBar(
                searchType: SearchType.games,
                title: 'Найти игру',
              ),
              SizedBox(
                height: 20,
              ),
              Expanded(child: GamesListing()),
            ],
          ),
        ),
      ),
    );
  }

  Widget _pageTitle() {
    return Text(
      'Список всех игр',
      style: TextStyle(
          fontSize: 23, fontWeight: FontWeight.bold, color: Colors.black),
    );
  }
}
