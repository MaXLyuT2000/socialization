import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:webview_flutter/webview_flutter.dart';

class GameWebView extends StatefulWidget {
  final String url;
  final bool useStatistics;

  const GameWebView({Key key, @required this.url, this.useStatistics})
      : super(key: key);

  @override
  _GameState createState() => _GameState();
}

class _GameState extends State<GameWebView> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
    ]);
    super.initState();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final String token = context.read<UserRepository>().token;
    return WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
          return;
        },
        //'http://mobile.itkostroma.ru/games/bdf231c9-8cfd-4339-aa6c-8a5c734ce089/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NTM5OTY3MzQsInVzZXIiOiJhMjZhYzU0OC0wZmRmLTQ5ZDAtODRlOC03NGIxNDc3OTdlZjcifQ.Gq3dqbSVMeV0EKD_QBFNunYpiZ9repA3JLsWk12uPuU'
        child: Scaffold(
          body: WebView(
            debuggingEnabled: true,
            initialUrl:
                'https://${widget.url}?token=$token&use_statistics=${widget.useStatistics}',
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
            onProgress: (int progress) {
              print('WebView is loading (progress : $progress%)');
            },
            javascriptChannels: <JavascriptChannel>{_endGameJSC(context)},
            backgroundColor: const Color(0x00000000),
          ),
        ));
  }

  JavascriptChannel _endGameJSC(BuildContext context) {
    return JavascriptChannel(
        name: 'endGame',
        onMessageReceived: (JavascriptMessage message) async {
          await Future.delayed(Duration(seconds: 1));
          Navigator.pop(context);
          print(message.message);
        });
  }
}
