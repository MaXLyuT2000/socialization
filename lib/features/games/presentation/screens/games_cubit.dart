import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/games/api/games_repository.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/games/presentation/screens/games_screen.dart';

class GamesListCubit extends StatelessWidget {
  const GamesListCubit({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider<GamesCubit>(
      create: (context) => GamesCubit(
        repository: context.read<GamesRepository>(),
        profile: context.read<UserRepository>(),
      ),
      child: GamesScreen(),
    );
  }
}
