import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/charts/presentation/screens/attempss_screen.dart';
import 'package:socialization/features/games/api/games_repository.dart';
import 'package:socialization/features/games/models/game_model.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/games/presentation/screens/game_webview.dart';
import 'package:socialization/features/patients_profile/presentation/widgets/game_status_button.dart';

import '../../../patients_profile/logic/cubit/patient_cubit.dart';
import '../../../patients_profile/logic/game_status/game_status_cubit.dart';

class GameInfo extends StatelessWidget {
  final Game game;
  const GameInfo({Key key, @required this.game}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final game = Game(
    //     name: 'Игра1',
    //     description:
    //         'Super Mario — серия компьютерных игр в жанре платформер, издаваемых компанией Nintendo. Часть медиафраншизы Mario. Первая игра в серии — Super Mario Bros. — вышла в 1985 году, последняя — Super Mario 3D World + Bowser’s Fury — в 2021 году. Серия Super Mario является основной линейкой игр в своей франшизе.');

    final style = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(100, 35)),
    );
    final role = context.read<UserRepository>().role;
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Padding(
            padding: EdgeInsets.only(left: 30, right: 30, bottom: 30),
            child: Column(children: [
              _buildGameImage(context, game.image),
              SizedBox(
                height: 20,
              ),
              Expanded(child: _buildInfo(game.name, game.description)),
              role == Roles.Patient
                  ? _buildPlayButton(context)
                  : _buildTutorButtons(context)
            ])),
      ),
    );
  }

  Widget _buildGameImage(BuildContext context, String image) {
    final height = MediaQuery.of(context).size.height * 0.4;
    return Container(
      alignment: Alignment.center,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: InkWell(
          child: image != null
              ? Image.network(
                  image,
                  fit: BoxFit.cover,
                  height: height,
                )
              : Image.asset(
                  'assets/empty_game.jpg',
                  fit: BoxFit.cover,
                  height: height,
                ),
        ),
      ),
    );
  }

  Widget _buildInfo(String name, String description) {
    return Column(
      children: [
        Text(
          name,
          style: TextStyle(
              fontSize: 23, fontWeight: FontWeight.bold, color: Colors.black),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          description,
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
      ],
    );
  }

  //Кнопки для Пациента
  Widget _buildPlayButton(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final textStyle = TextStyle(fontSize: 18);
    final buttonStyle = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(width - 100, 50)),
    );
    return ElevatedButton(
      onPressed: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return GameWebView(
            url: game.link,
            useStatistics: game.useStatistics,
          );
        }));
      },
      style: buttonStyle,
      child: Text('Начать игру', style: textStyle),
    );
  }

  //Кнопки дял Тьютора
  Widget _buildTutorButtons(BuildContext context) {
    final textStyle = TextStyle(fontSize: 18);
    final width = MediaQuery.of(context).size.width;
    final buttonStyle = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(width - 100, 50)),
    );

    return Column(
      children: [
        ElevatedButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return GameWebView(
                url: game.link,
                useStatistics: game.useStatistics,
              );
            }));
          },
          style: buttonStyle,
          child: Text(
            'Показать игру',
          ),
        ),
        SizedBox(
          height: 20,
        ),
        context.read<GamesCubit>().getAllAvailable
            ? Text('')
            : ElevatedButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return AttempsScreen(
                      gameId: game.gamePatientId,
                    );
                  }));
                },
                style: buttonStyle,
                child: Text(
                  'Статистика',
                ),
              ),
        SizedBox(
          height: 20,
        ),
        MultiBlocProvider(
          providers: [
            BlocProvider<GameStatusCubit>(
              create: (context) => GameStatusCubit(
                  repository: context.read<GamesRepository>(),
                  token: context.read<UserRepository>().token,
                  patientId: context.read<PatientCubit>().patientId,
                  gameId: game.id,
                  gameStatusDetached:
                      context.read<GamesCubit>().getAllAvailable),
            ),
            BlocProvider.value(
              value: context.read<GamesCubit>(),
            )
          ],
          child: GameStatusButton(
            gameId: game.id,
            buttonStyle: buttonStyle,
          ),
        ),
      ],
    );
  }
}
