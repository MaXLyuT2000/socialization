import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:socialization/common/app_settings.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/games/models/game_model.dart';
import 'package:socialization/features/patients/models/filter_model.dart';

class GamesRepository {
  static const String mainUrl = AppSettengs.apiUrl;
  static const gamesAllUrl = '$mainUrl/games/all';
  static const gamesPatientUrl = '$mainUrl/patient/games/getting';
  static const gameAddingUrl = '$mainUrl/tutor/patients/games/adding';
  static const gameRemoveingUrl = '$mainUrl/tutor/patients/games/removing';
  //Игры назначенные пациенту
  static const gamesTutorPatientOneUrl =
      '$mainUrl/tutor/patients/getting/one/games';
  //Все игры кроме тех,которые уже есть
  static const gamesTutorPatientOneAllUrl = '$mainUrl/games/all';

  final int limit = 10;

  final Dio _dio = Dio();
  final FlutterSecureStorage storage = new FlutterSecureStorage();

  Future<String> getToken() async {
    var value = await storage.read(key: 'token');
    return value;
  }

  //Получение всех игры системы
  Future<GamesPage> getAllGames(
      {String regex,
      @required String token,
      @required int page,
      @required Roles role,
      @required FilterCategory category}) async {
    String url;
    switch (role) {
      case Roles.Tutor:
        url = gamesAllUrl;
        break;
      case Roles.Patient:
        url = gamesPatientUrl;
        break;
      default:
    }

    Response response = await _dio.get(url,
        queryParameters: {
          'limit': limit,
          'start': page,
          'regex': regex,
          'order': category.idCategory
        },
        options: Options(headers: {
          "token": token,
        }));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      var games = jsonObject['results'] as List<dynamic>;
      return GamesPage(
          next: jsonObject['next'],
          games: games.map((json) => Game.fromJson(json)).toList());
    } else
      return null;
  }

  Future<GamesPage> getTutorPatientGames(
      {String regex,
      @required String token,
      @required int page,
      @required String patientId,
      @required bool getAllAvailable}) async {
    Response response = await _dio.get(
        getAllAvailable ? gamesTutorPatientOneAllUrl : gamesTutorPatientOneUrl,
        queryParameters: {
          'limit': limit,
          'start': page,
          'regex': regex,
          'patientID': patientId
        },
        options: Options(headers: {
          "token": token,
        }));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      var games = jsonObject['results'] as List<dynamic>;
      return GamesPage(
          next: jsonObject['next'],
          games: games.map((json) => Game.fromJson(json)).toList());
    } else
      return null;
  }

  //Привязка пациента к тьтору по patientId
  Future<bool> postGameAttaching(
      {@required String token,
      @required String patientId,
      @required String gameId}) async {
    Response response = await _dio.post(gameAddingUrl,
        data: {"game_id": gameId, "patient_id": patientId},
        options: Options(headers: {
          "token": token,
        }));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      return true;
    } else
      return false;
  }

  //Отвязка пациента к тьтору по patientId
  Future<bool> deleteGameDetaching(
      {@required String token,
      @required String patientId,
      @required String gameId}) async {
    Response response = await _dio.delete(gameRemoveingUrl,
        data: {"game_id": gameId, "patient_id": patientId},
        options: Options(headers: {
          "token": token,
        }));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      return true;
    } else
      return false;
  }
}
