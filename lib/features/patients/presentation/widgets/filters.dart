import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/patients/models/filter_model.dart';
import 'package:socialization/features/patients/presentation/patients/patients_cubit.dart';
import 'package:socialization/features/patients/presentation/patients_add/patientsAdd_cubit.dart';
import 'package:socialization/features/patients/presentation/widgets/search_bar.dart';

enum GenderList { alphabet, date, female }

class FiltersPopUp extends StatefulWidget {
  final SearchType searchType;

  FiltersPopUp({Key key, @required this.searchType}) : super(key: key);

  @override
  State<FiltersPopUp> createState() => _FiltersPopUpState();
}

class _FiltersPopUpState extends State<FiltersPopUp> {
  final _filterController = TextEditingController();
  List<FilterCategory> _filterCategories = [];
  FilterCategory _selectedFilter;
  @override
  void initState() {
    _getFilterBySearchType();
    super.initState();
  }

  @override
  void dispose() {
    _filterController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final buttonStyle = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(width, 50)),
    );
    final textStyle = TextStyle(fontSize: 20, color: Colors.white);

    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Material(
        color: Colors.transparent,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(40)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Фильтры',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    FiltersRadioButtons(
                      filters: _filterCategories,
                      onFilterChanged: (filter) {
                        _selectedFilter = filter;
                      },
                      selectedFilter: _selectedFilter,
                    ),
                    SizedBox(
                      height: 50,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 50,
              ),
              ElevatedButton(
                  style: buttonStyle,
                  onPressed: () {
                    _onApplyFilter(_selectedFilter);
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Применить',
                    style: textStyle,
                  ))
            ],
          ),
        ),
      ),
    );
  }

  void _getFilterBySearchType() {
    switch (widget.searchType) {
      case SearchType.patients:
        _filterCategories = BlocProvider.of<PatientsCubit>(context).filters;
        _selectedFilter =
            BlocProvider.of<PatientsCubit>(context).selectedFilterCategory;
        break;
      case SearchType.pateintsAdd:
        _filterCategories = BlocProvider.of<PatientsAddCubit>(context).filters;
        _selectedFilter =
            BlocProvider.of<PatientsAddCubit>(context).selectedFilterCategory;
        break;
      case SearchType.games:
        _filterCategories = BlocProvider.of<GamesCubit>(context).filters;
        _selectedFilter =
            BlocProvider.of<GamesCubit>(context).selectedFilterCategory;
        break;
      default:
        return null;
    }
  }

  void _onApplyFilter(FilterCategory filterCategory) {
    switch (widget.searchType) {
      case SearchType.patients:
        BlocProvider.of<PatientsCubit>(context).sortByFilter(filterCategory);
        break;
      case SearchType.pateintsAdd:
        BlocProvider.of<PatientsAddCubit>(context).sortByFilter(filterCategory);
        break;
      case SearchType.games:
        BlocProvider.of<GamesCubit>(context).sortByFilter(filterCategory);
        break;
      default:
    }
  }
}

class FiltersRadioButtons extends StatefulWidget {
  final Function(FilterCategory) onFilterChanged;
  final List<FilterCategory> filters;
  final FilterCategory selectedFilter;
  FiltersRadioButtons(
      {Key key,
      this.onFilterChanged,
      @required this.filters,
      @required this.selectedFilter})
      : super(key: key);

  @override
  State<FiltersRadioButtons> createState() => _FiltersRadioButtonsState();
}

class _FiltersRadioButtonsState extends State<FiltersRadioButtons> {
  FilterCategory _selectedFilter;
  @override
  void initState() {
    super.initState();
    _selectedFilter = widget.selectedFilter;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: widget.filters.length,
        itemBuilder: ((context, index) {
          return RadioListTile(
            title: Text(widget.filters[index].nameCategory),
            value: widget.filters[index],
            groupValue: _selectedFilter,
            onChanged: (FilterCategory value) {
              setState(() {
                widget.onFilterChanged(value);
                _selectedFilter = value;
              });
            },
          );
        }));
  }
}
