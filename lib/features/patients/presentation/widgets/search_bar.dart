import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/patients/presentation/patients/patients_cubit.dart';
import 'package:socialization/features/patients/presentation/patients_add/patientsAdd_cubit.dart';
import 'package:socialization/features/patients/presentation/widgets/filters.dart';

enum SearchType { patients, pateintsAdd, games }

class SearchBar extends StatefulWidget {
  final SearchType searchType;
  final String title;
  const SearchBar({Key key, @required this.searchType, this.title = 'Поиск'})
      : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final _controller = TextEditingController();

  @override
  void initState() {
    _controller.addListener(() {
      switch (widget.searchType) {
        case SearchType.patients:
          BlocProvider.of<PatientsCubit>(context)
              .searchPatient(_controller.text);
          break;
        case SearchType.pateintsAdd:
          BlocProvider.of<PatientsAddCubit>(context)
              .searchPatient(_controller.text);
          break;
        case SearchType.games:
          BlocProvider.of<GamesCubit>(context).searchGames(_controller.text);
          break;
        default:
      }
    });
    super.initState();
  }

  Widget createBlocProviderByCategory(BuildContext context) {
    switch (widget.searchType) {
      case SearchType.patients:
        return FiltersPopUp(
          searchType: widget.searchType,
        );
        break;
      case SearchType.pateintsAdd:
        return BlocProvider.value(
          value: context.read<PatientsAddCubit>(),
          child: FiltersPopUp(
            searchType: widget.searchType,
          ),
        );
        break;
      case SearchType.games:
        return BlocProvider.value(
          value: context.read<GamesCubit>(),
          child: FiltersPopUp(
            searchType: widget.searchType,
          ),
        );
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            // padding: const EdgeInsets.only(left: 25, right: 25),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              borderRadius: BorderRadius.circular(25),
              color: Colors.white,
            ),
            child: Row(
              children: [
                Expanded(
                    child: TextField(
                  textAlign: TextAlign.center,
                  controller: _controller,
                  decoration: InputDecoration(
                    hintText: widget.title,
                    border: InputBorder.none,
                  ),
                )),
                IconButton(
                    icon: const Icon(
                      Icons.search,
                    ),
                    onPressed: () {}),
                IconButton(
                    icon: const Icon(
                      Icons.menu,
                    ),
                    onPressed: () async {
                      await showGeneralDialog(
                          context: context,
                          pageBuilder: (_, __, ___) {
                            return createBlocProviderByCategory(context);
                          });
                    }),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
