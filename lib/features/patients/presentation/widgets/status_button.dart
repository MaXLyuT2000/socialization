import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/patients/presentation/patient_status/patient_status_cubit.dart';
import 'package:socialization/features/patients/presentation/patients/patients_cubit.dart';

class StatusButton extends StatelessWidget {
  final String patientId;
  final bool addPatient;
  final ButtonStyle buttonStyle;
  const StatusButton(
      {Key key,
      @required this.patientId,
      this.addPatient = false,
      this.buttonStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PatientStatusCubit, PatientStatusState>(
      listener: (context, state) {
        if (state is PatientStatusAttached) {
          BlocProvider.of<PatientsCubit>(context).refreshData();
        }
        if (state is PatientStatusDetached) {
          BlocProvider.of<PatientsCubit>(context).refreshData();
        }
      },
      builder: (context, state) {
        return BlocBuilder<PatientStatusCubit, PatientStatusState>(
          builder: (context, state) {
            if (state is PatientStatusDetached) {
              return ElevatedButton(
                style: buttonStyle,
                onPressed: () {
                  BlocProvider.of<PatientStatusCubit>(context)
                      .attachPatient(patientId: patientId);
                },
                child: Text('Добавить'),
              );
            }

            if (state is PatientStatusAttached) {
              return ElevatedButton(
                style: buttonStyle,
                onPressed: () {
                  BlocProvider.of<PatientStatusCubit>(context)
                      .detachPatient(patientId: patientId);
                },
                child: Text('Удалить'),
              );
            }

            if (state is PatientStatusLoading) {
              return ElevatedButton(
                style: buttonStyle,
                onPressed: () {},
                child: CircularProgressIndicator(),
              );
            }

            return Text('data');
          },
        );
      },
    );
  }
}
