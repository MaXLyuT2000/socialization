import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:socialization/features/patients/presentation/screens/patients_add/patients_listing.dart';
import 'package:socialization/features/patients/presentation/widgets/search_bar.dart';

class PatientsAddScreen extends StatelessWidget {
  const PatientsAddScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarIconBrightness: Brightness.dark,
      statusBarBrightness: Brightness.dark,
      //При темной теме затемняет
      statusBarIconBrightness: Brightness.light,
      statusBarColor: Colors.transparent,
      // systemNavigationBarColor: Colors.red,
      // systemNavigationBarDividerColor: Colors.blue,
    ));
    return Scaffold(
      appBar: AppBar(
        title: _pageTitle(),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              SearchBar(
                searchType: SearchType.pateintsAdd,
                title: 'Найти пациента',
              ),
              SizedBox(
                height: 20,
              ),
              Expanded(child: PatientsAddListing()),
            ],
          ),
        ),
      ),
    );
  }

  Widget _pageTitle() {
    return Text(
      'Все наблюдаемые ',
      style: TextStyle(
          fontSize: 23, fontWeight: FontWeight.bold, color: Colors.black),
    );
  }
}
