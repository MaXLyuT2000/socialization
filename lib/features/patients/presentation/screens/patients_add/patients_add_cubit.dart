import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/patients/api/patient_repository.dart';
import 'package:socialization/features/patients/presentation/patients_add/patientsAdd_cubit.dart';
import 'package:socialization/features/patients/presentation/screens/patients_add/patients_screen.dart';

class PatientsAddListCubit extends StatelessWidget {
  const PatientsAddListCubit({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PatientsAddCubit>(
      create: (context) => PatientsAddCubit(
        repository: context.read<PatientsRepository>(),
        token: context.read<UserRepository>().token,
      ),
      child: PatientsAddScreen(),
    );
  }
}
