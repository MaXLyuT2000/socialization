import 'package:flutter/material.dart';
import 'package:socialization/features/patients/presentation/screens/patients_list/patients_listing.dart';
import 'package:socialization/features/patients/presentation/widgets/search_bar.dart';

class PatientsScreen extends StatelessWidget {
  const PatientsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: _pageTitle()),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                SearchBar(
                  searchType: SearchType.patients,
                  title: 'Найти пациента',
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(child: PatientsListing()),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pushNamed(context, '/PatientsAdd');
            },
            child: Icon(
              Icons.add,
              color: Theme.of(context).primaryColor,
            )));
  }

  Widget _pageTitle() {
    return Text(
      'Наблюдаемые',
      style: TextStyle(
          fontSize: 23, fontWeight: FontWeight.bold, color: Colors.black),
    );
  }
}
