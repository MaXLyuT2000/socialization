import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/patients/api/patient_repository.dart';
import 'package:socialization/features/patients/models/patient_model.dart';
import 'package:socialization/features/patients/presentation/patient_status/patient_status_cubit.dart';
import 'package:socialization/features/patients/presentation/widgets/status_button.dart';
import 'package:socialization/features/patients_profile/presentation/screens/patient_info_cubit.dart';

class PatientCard extends StatelessWidget {
  final Patient patient;
  final bool show;
  final addPatient;
  const PatientCard(
      {Key key, this.patient, this.show = true, @required this.addPatient})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final style = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(100, 35)),
    );
    final _buttonCubit = PatientStatusCubit(
        repository: context.read<PatientsRepository>(),
        token: context.read<UserRepository>().token,
        addPatient: addPatient);

    return BlocProvider<PatientStatusCubit>.value(
        value: _buttonCubit,
        child: GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => BlocProvider.value(
                          value: _buttonCubit,
                          child: PatientInfoScreenCubit(
                            patient: patient,
                          ),
                        )));
          },
          child: Column(
            children: [
              _profileImage(patient.image),
              SizedBox(
                height: 10,
              ),
              _buildName(patient),
              show
                  ? StatusButton(
                      patientId: patient.id,
                      buttonStyle: style,
                    )
                  : Text(''),
            ],
          ),
        ));
  }

  Widget _profileImage(String image) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: Container(
        height: 200,
        child: AspectRatio(
          aspectRatio: 3 / 4,
          child: image != null
              ? Image.network(
                  patient.image,
                  fit: BoxFit.cover,
                  height: 180,
                )
              : Image.asset(
                  'assets/profile_avatar.jpg',
                  fit: BoxFit.cover,
                  height: 180,
                ),
        ),
      ),
    );
  }

  Widget _buildName(Patient patient) {
    return Text(
      patient.name + " " + patient.surname,
      maxLines: 2,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
