import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/models/profile.dart';
import 'package:socialization/features/patients/presentation/patients/patients_cubit.dart';
import 'package:socialization/features/patients/presentation/screens/patients_list/patient_card.dart';

class PatientsListing extends StatelessWidget {
  final scrollController = ScrollController();

  PatientsListing({Key key}) : super(key: key);

  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<PatientsCubit>(context).loadPosts();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    // BlocProvider.of<PatientsCubit>(context).loadPosts();
    return BlocBuilder<PatientsCubit, PatientsState>(builder: (context, state) {
      Future _refreshData() async {
        BlocProvider.of<PatientsCubit>(context).refreshData();
      }

      if (state is PatientsInitial) {
        _refreshData();
      }

      if (state is PatientsLoading && state.isFirstFetch) {
        return _loadingIndicator();
      }

      List<User> posts = [];
      bool isLoading = false;

      if (state is PatientsLoading) {
        posts = state.oldPatients;
        isLoading = true;
      } else if (state is PatientsLoaded) {
        posts = state.patients;
      }

      return RefreshIndicator(
        onRefresh: _refreshData,
        child: posts.isEmpty
            ? _emptyList()
            : GridView.builder(
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 2 / 3.5,
                    crossAxisCount: 2,
                    crossAxisSpacing: 30),
                physics: const AlwaysScrollableScrollPhysics(),
                controller: scrollController,
                itemBuilder: (context, index) {
                  if (index < posts.length) {
                    return PatientCard(
                      patient: posts[index],
                      addPatient: false,
                      show: false,
                    );
                  } else {
                    Timer(const Duration(milliseconds: 30), () {
                      scrollController
                          .jumpTo(scrollController.position.maxScrollExtent);
                    });

                    return _loadingIndicator();
                  }
                },
                itemCount: posts.length + (isLoading ? 1 : 0),
              ),
      );
    });
  }

  Widget _emptyList() {
    return Center(
      child: Text('Список пациентов пуст'),
    );
  }

  Widget _loadingIndicator() {
    return const Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }
}
