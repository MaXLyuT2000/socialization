part of 'patientsAdd_cubit.dart';

abstract class PatientsAddState extends Equatable {
  const PatientsAddState();

  @override
  List<Object> get props => [];
}

class PatientsAddInitial extends PatientsAddState {}

class PatientsAddLoaded extends PatientsAddState {
  List<Patient> patients;

  PatientsAddLoaded(this.patients);
}

class PatientsAddLoading extends PatientsAddState {
  final List<Patient> oldPatients;
  final bool isFirstFetch;

  PatientsAddLoading(this.oldPatients, {this.isFirstFetch = false});
}
