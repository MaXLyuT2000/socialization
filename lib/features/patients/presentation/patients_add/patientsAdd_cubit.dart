import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:socialization/features/patients/api/patient_repository.dart';
import 'package:socialization/features/patients/models/filter_model.dart';
import 'package:socialization/features/patients/models/patient_model.dart';

part 'patientsAdd_state.dart';

class PatientsAddCubit extends Cubit<PatientsAddState> {
  final PatientsRepository repository;
  final String token;
  final List<FilterCategory> filters = [
    FilterCategory(idCategory: '1', nameCategory: 'По дате'),
    FilterCategory(idCategory: '2', nameCategory: 'По алфавиту'),
    FilterCategory(idCategory: '3', nameCategory: 'По дате рождения'),
  ];

  String patientSearchRegex = '';
  FilterCategory selectedFilterCategory =
      FilterCategory(idCategory: '1', nameCategory: 'По дате');

  PatientsAddCubit({
    @required this.repository,
    @required this.token,
  }) : super(PatientsAddInitial());

  int page = 0;
  bool allLoaded = false;

  void loadPosts() {
    if ((state is PatientsAddLoading) || (allLoaded)) return;

    final currentState = state;

    var oldPatients = <Patient>[];
    if (currentState is PatientsAddLoaded) {
      oldPatients = currentState.patients;
    }

    emit(PatientsAddLoading(oldPatients, isFirstFetch: page == 0));

    repository
        .getPatients(
            allPateints: true,
            regex: patientSearchRegex,
            page: page,
            token: token,
            category: selectedFilterCategory)
        .then((newPage) {
      if (newPage.next == false) {
        allLoaded = true;
      }
      page++;

      oldPatients.addAll(newPage.pateints);

      emit(PatientsAddLoaded(oldPatients));
    });
  }

  void searchPatient(String regex) async {
    page = 0;
    patientSearchRegex = regex;
    allLoaded = false;

    emit(PatientsAddInitial());
    loadPosts();
  }

  //Поиск по фильтру
  void sortByFilter(FilterCategory filterCategory) {
    selectedFilterCategory = filterCategory;
    refreshData();
  }

  void refreshData() {
    final currentState = state;
    if (currentState is PatientsAddLoaded) {
      currentState.patients = [];
    }
    page = 0;
    allLoaded = false;
    loadPosts();
  }
}
