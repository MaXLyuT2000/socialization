part of 'patients_cubit.dart';

abstract class PatientsState extends Equatable {
  const PatientsState();

  @override
  List<Object> get props => [];
}

class PatientsInitial extends PatientsState {}

class PatientsLoaded extends PatientsState {
  List<Patient> patients;

  PatientsLoaded(this.patients);
}

class PatientsLoading extends PatientsState {
  final List<Patient> oldPatients;
  final bool isFirstFetch;

  PatientsLoading(this.oldPatients, {this.isFirstFetch = false});
}
