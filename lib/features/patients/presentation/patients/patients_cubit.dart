import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:socialization/features/patients/api/patient_repository.dart';
import 'package:socialization/features/patients/models/filter_model.dart';
import 'package:socialization/features/patients/models/patient_model.dart';

part 'patients_state.dart';

class PatientsCubit extends Cubit<PatientsState> {
  final PatientsRepository repository;
  final String token;
  final bool allPateints;
  final List<FilterCategory> filters = [
    FilterCategory(idCategory: '1', nameCategory: 'По дате'),
    FilterCategory(idCategory: '2', nameCategory: 'По алфавиту'),
    FilterCategory(idCategory: '3', nameCategory: 'По дате рождения'),
  ];

  String patientSearchRegex = '';
  FilterCategory selectedFilterCategory =
      FilterCategory(idCategory: '1', nameCategory: 'По дате');

  PatientsCubit({
    @required this.repository,
    @required this.token,
    this.allPateints = false,
  }) : super(PatientsInitial());

  int page = 0;
  bool allLoaded = false;

  void loadPosts() {
    if ((state is PatientsLoading) || (allLoaded)) return;

    final currentState = state;

    var oldPatients = <Patient>[];
    if (currentState is PatientsLoaded) {
      oldPatients = currentState.patients;
    }

    emit(PatientsLoading(oldPatients, isFirstFetch: page == 0));

    repository
        .getPatients(
            allPateints: allPateints,
            regex: patientSearchRegex,
            page: page,
            token: token,
            category: selectedFilterCategory)
        .then((newPage) {
      if (newPage.next == false) {
        allLoaded = true;
      }
      page++;

      // final museums = (state as MuseumsLoading).oldMuseums;
      // museums.addAll(newPage.museums);
      oldPatients.addAll(newPage.pateints);

      emit(PatientsLoaded(oldPatients));
    });
  }

  void searchPatient(String regex) async {
    page = 0;
    patientSearchRegex = regex;
    allLoaded = false;

    emit(PatientsInitial());
    loadPosts();
  }

  //Поиск по фильтру
  void sortByFilter(FilterCategory filterCategory) {
    selectedFilterCategory = filterCategory;
    refreshData();
  }

  void refreshData() {
    final currentState = state;
    if (currentState is PatientsLoaded) {
      currentState.patients = [];
    }
    page = 0;
    allLoaded = false;
    loadPosts();
  }
}
