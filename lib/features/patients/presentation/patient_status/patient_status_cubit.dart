import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:socialization/features/patients/api/patient_repository.dart';

part 'patient_status_state.dart';

class PatientStatusCubit extends Cubit<PatientStatusState> {
  final PatientsRepository repository;
  final String token;
  //false - отвязка пациента true - доавбление
  final bool addPatient;
  PatientStatusCubit({
    @required this.repository,
    @required this.token,
    this.addPatient = false,
  }) : super(addPatient ? PatientStatusDetached() : PatientStatusAttached());

  void attachPatient({@required String patientId}) async {
    emit(PatientStatusLoading());
    await repository
        .postPatientAttaching(patientId: patientId, token: token)
        .then((status) {
      status ? emit(PatientStatusAttached()) : emit(PatientStatusDetached());
    });
  }

  void detachPatient({@required String patientId}) async {
    emit(PatientStatusLoading());
    await repository
        .deletePatientDetaching(patientId: patientId, token: token)
        .then((status) {
      status ? emit(PatientStatusDetached()) : emit(PatientStatusAttached());
    });
  }
}
