part of 'patient_status_cubit.dart';

abstract class PatientStatusState extends Equatable {
  const PatientStatusState();

  @override
  List<Object> get props => [];
}

class PatientStatusLoading extends PatientStatusState {}

class PatientStatusAttached extends PatientStatusState {}

class PatientStatusDetached extends PatientStatusState {}
