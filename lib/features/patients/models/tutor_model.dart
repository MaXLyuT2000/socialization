import 'package:socialization/features/authorization/models/profile.dart';

class Tutor extends User {
  Tutor(
      {id,
      name,
      surname,
      secondname,
      organization,
      email,
      numberPhone,
      gender,
      birthDate,
      image})
      : super(
            id: id,
            name: name,
            surname: surname,
            secondName: secondname,
            organization: organization,
            email: email,
            birthDate: birthDate,
            numberPhone: numberPhone,
            gender: gender,
            image: image);

  @override
  factory Tutor.fromJson(Map<String, dynamic> json) {
    return Tutor(
      id: json['id'] ?? null,
      name: json['name'] ?? 'Неизвестно',
      surname: json['surname'] ?? 'Неизвестно',
      secondname: json['surname'] ?? 'Неизвестно',
      email: json['surname'] ?? 'Неизвестно',
      image: json['image'] == null
          ? ''
          : 'https://mobile.itkostroma.ru/images/' + json['image'],
      organization: json['organization'] ?? 'Неизвестно',
      gender: json['gender'] ?? 'Неизвестно',
      numberPhone: json['numberPhone'] ?? 'Неизвестно',
      birthDate: json['birthDate'] ?? 'Неизвестно',
    );
  }
}
