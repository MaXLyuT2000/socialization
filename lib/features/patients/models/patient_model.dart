import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:socialization/features/authorization/models/profile.dart';
import 'package:socialization/features/games/models/game_model.dart';
import 'package:socialization/features/patients/models/tutor_model.dart';

class Patient extends User {
  final List<Game> games;
  final Tutor tutor;

  Patient(
      {@required String id,
      @required String name,
      @required String surname,
      @required String secondName,
      @required String image,
      @required String organization,
      @required String email,
      @required String numberPhone,
      @required String gender,
      @required String birthDate,
      @required this.tutor,
      @required this.games})
      : super(
            id: id,
            name: name,
            surname: surname,
            secondName: secondName,
            organization: organization,
            email: email,
            birthDate: birthDate,
            numberPhone: numberPhone,
            gender: gender,
            image: image);

  factory Patient.fromJson(Map<String, dynamic> json) {
    final List<dynamic> gamesList = json['games'];
    return Patient(
      id: json['id'],
      name: json['name'] ?? 'Неизвестно',
      surname: json['surname'] ?? 'Неизвестно',
      secondName: json['secondName'] ?? 'Неизвестно',
      image: json['image'] == null
          ? null
          : 'https://mobile.itkostroma.ru/images/' + json['image'],
      organization: json['organization'] ?? 'Неизвестно',
      email: json['email'],
      numberPhone: json['numberPhone'] ?? 'Неизвестно',
      gender: json['gender'] ?? 'Неизвестно',
      birthDate: json['birthDate'] != null
          ? DateFormat('dd.MM.yyyy').format(DateTime.parse(json['birthDate']))
          : 'Неизвестно',
      //Params for patients class
      tutor: json['tutor'] == null ? null : Tutor.fromJson(json['tutor']),
      games: gamesList == null
          ? null
          : gamesList.map((game) => Game.fromJson(game)).toList(),
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => [id, name, surname, organization];
}

//Модель пагинации
class PatientsPage {
  final bool next;
  final List<Patient> pateints;

  PatientsPage({this.next, this.pateints});
}
