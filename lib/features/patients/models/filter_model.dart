import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class FilterCategory extends Equatable {
  final String idCategory;
  final String nameCategory;

  FilterCategory({@required this.idCategory, @required this.nameCategory});

  @override
  // TODO: implement props
  List<Object> get props => [idCategory, nameCategory];
}
