import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:socialization/common/app_settings.dart';
import 'package:socialization/features/patients/models/filter_model.dart';
import 'package:socialization/features/patients/models/patient_model.dart';

class PatientsRepository {
  static const String mainUrl = AppSettengs.apiUrl;
  static const patientsUrl = '$mainUrl/tutor/patients/getting';
  static const patientsAllUrl = '$mainUrl/tutor/patients/getting/all';
  static const patientInfoUrl = '$mainUrl/tutor/patients/getting/one';
  static const patientAttaching = '$mainUrl/tutor/patients/attaching';
  static const patientDetaching = '$mainUrl/tutor/patients/detaching';
  final int limit = 10;

  final Dio _dio = Dio();

  // Получение списка пациентов прикрепленных к тьютору.
  Future<PatientsPage> getPatients(
      {String regex,
      @required String token,
      @required int page,
      @required bool allPateints,
      @required FilterCategory category}) async {
    Response response = await _dio.get(
        allPateints ? patientsAllUrl : patientsUrl,
        queryParameters: {
          'limit': limit,
          'start': page,
          'regex': regex,
          'order': category.idCategory
        },
        options: Options(headers: {
          "token": token,
        }));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      var pateints = jsonObject['results'] as List<dynamic>;
      return PatientsPage(
          next: jsonObject['next'],
          pateints: pateints.map((json) => Patient.fromJson(json)).toList());
    } else
      return null;
  }

  //Получение подробной информации о пациенте прикрепленного к тьютору
  Future<Patient> getPatientInfo(
      {@required String token, @required String patientId}) async {
    Response response = await _dio.get(patientInfoUrl,
        queryParameters: {"patientID": patientId},
        options: Options(headers: {
          "token": token,
        }));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      return Patient.fromJson(jsonObject);
    } else
      return null;
  }

  //Привязка пациента к тьтору по patientId
  Future<bool> postPatientAttaching(
      {@required String token, @required String patientId}) async {
    Response response = await _dio.post(patientAttaching,
        data: {"patient": patientId},
        options: Options(headers: {
          "token": token,
        }));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      return true;
    } else
      return false;
  }

  //Отвязка пациента к тьтору по patientId
  Future<bool> deletePatientDetaching(
      {@required String token, @required String patientId}) async {
    Response response = await _dio.delete(patientDetaching,
        data: {"patient": patientId},
        options: Options(headers: {
          "token": token,
        }));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      return true;
    } else
      return false;
  }
}
