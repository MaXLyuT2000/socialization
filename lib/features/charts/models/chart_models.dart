class ChartData {
  final String lvlName;

  ChartData(this.lvlName);
}

//Модель кликов:
class ChartClicksData extends ChartData {
  final int clicks;
  final int missclicks;

  ChartClicksData({lvlName, this.clicks, this.missclicks}) : super(lvlName);

  factory ChartClicksData.fromJson(Map<String, dynamic> json) {
    return ChartClicksData(
        lvlName: json['level_names'],
        clicks: json['clicks'],
        missclicks: json['missclicks']);
  }
}

//Модель тветов
class ChartAnswersData extends ChartData {
  final int incorrect;
  final int correct;

  ChartAnswersData({lvlName, this.incorrect, this.correct}) : super(lvlName);

  factory ChartAnswersData.fromJson(Map<String, dynamic> json) {
    return ChartAnswersData(
      lvlName: json['level_names'],
      correct: json['correct'],
      incorrect: json['incorrect'],
    );
  }
}

//Модель тайм лайн
class ChartTimeLineData extends ChartData {
  final int spendTime;

  ChartTimeLineData({
    lvlName,
    this.spendTime,
  }) : super(lvlName);

  factory ChartTimeLineData.fromJson(Map<String, dynamic> json) {
    return ChartTimeLineData(
        lvlName: json['level_names'], spendTime: json['spend_time']);
  }
}

//Модель общего времени
class ChartTimeSummData extends ChartData {
  final int spendTime;

  ChartTimeSummData({lvlName, this.spendTime}) : super(lvlName);

  factory ChartTimeSummData.fromJson(Map<String, dynamic> json) {
    return ChartTimeSummData(
        lvlName: json['level_names'], spendTime: json['spend_time']);
  }
}
