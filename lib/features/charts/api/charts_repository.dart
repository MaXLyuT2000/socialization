import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:socialization/common/app_settings.dart';

class ChartsRepository {
  static const String mainUrl = AppSettengs.domenUrl;
  //Попытки
  static const getAttemps = '$mainUrl/api/statistic_pagination';
  //Виджеты
  static const getTimeSumm = '$mainUrl/api/all_time_widget';
  static const getClicks = '$mainUrl/api/clicks_widget';
  static const getAnswers = '$mainUrl/api/answers_widget';
  static const getTimeLine = '$mainUrl/api/timeline_widget';

  final Dio _dio = Dio();

  Future<List<String>> getStatAttemps(String gameId) async {
    Response response =
        await _dio.get(getAttemps, queryParameters: {'gp_id': gameId});
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.data);
      return List<String>.from(jsonObject['attempts'] as List);
    }
  }

  Future<dynamic> getStatClicks(String attempId) async {
    Response response = await _dio.get(getClicks,
        queryParameters: {'sg_id': attempId, 'source': 'mobile'});
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.data);
      return jsonObject;
    }
  }

  Future<dynamic> getStatAnswers(String attempId) async {
    Response response = await _dio.get(getAnswers,
        queryParameters: {'sg_id': attempId, 'source': 'mobile'});
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.data);
      return jsonObject;
    }
  }

  Future<dynamic> getStatTimeLineAnswers(String attempId) async {
    Response response = await _dio.get(getTimeLine,
        queryParameters: {'sg_id': attempId, 'source': 'mobile'});
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.data);
      return jsonObject;
    }
  }

  Future<dynamic> getStatTimeSummAnswers(String attempId) async {
    Response response = await _dio.get(getTimeSumm,
        queryParameters: {'sg_id': attempId, 'source': 'mobile'});
    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.data);
      return jsonObject;
    }
  }
}
