part of 'attemps_cubit.dart';

abstract class AttempsState extends Equatable {
  const AttempsState();

  @override
  List<Object> get props => [];
}

class AttempsInitial extends AttempsState {}

class AttempsLoading extends AttempsState {}

class AttempsLoaded extends AttempsState {
  final List<String> attemps;

  AttempsLoaded(this.attemps);
}
