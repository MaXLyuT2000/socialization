import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:socialization/features/charts/api/charts_repository.dart';

part 'attemps_state.dart';

class AttempsCubit extends Cubit<AttempsState> {
  final ChartsRepository repository;
  AttempsCubit(this.repository) : super(AttempsInitial());

  void getAttemps(String gameId) async {
    emit(AttempsLoading());
    var attemps = await repository.getStatAttemps(gameId);
    emit(AttempsLoaded(attemps));
  }
}
