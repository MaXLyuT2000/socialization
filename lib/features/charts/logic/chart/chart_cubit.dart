import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:socialization/features/charts/api/charts_repository.dart';
import 'package:socialization/features/charts/models/chart_models.dart';

part 'chart_state.dart';

enum ChartType { clicks, answers, timeLine, timeSumm }

class ChartCubit extends Cubit<ChartState> {
  final ChartsRepository repository;
  final ChartType chartType;
  ChartCubit(this.repository, this.chartType) : super(ChartInitial());

  void getChartData(String attempId) async {
    switch (chartType) {
      case ChartType.clicks:
        var jsonData = await repository.getStatClicks(attempId);
        var values = jsonData['values'] as List<dynamic>;
        var list =
            values.map((data) => ChartClicksData.fromJson(data)).toList();
        emit(ChartLoaded(list));
        break;
      case ChartType.answers:
        var jsonData = await repository.getStatAnswers(attempId);
        var values = jsonData['values'] as List<dynamic>;
        var list =
            values.map((data) => ChartAnswersData.fromJson(data)).toList();

        emit(ChartLoaded(list));
        break;
      // case ChartType.timeLine:
      //   var jsonData = await repository.getStatClicks(attempId);
      //   emit(ChartClicksLoaded(ChartClicksData.fromJson(jsonData)));
      //   break;
      case ChartType.timeSumm:
        var jsonData = await repository.getStatTimeSummAnswers(attempId);
        var values = jsonData['values'] as List<dynamic>;
        var list =
            values.map((data) => ChartTimeSummData.fromJson(data)).toList();

        emit(ChartLoaded(list));
        break;
      default:
    }
  }
}
