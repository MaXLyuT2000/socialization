/// Line chart example
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/charts/api/charts_repository.dart';
import 'package:socialization/features/charts/logic/chart/chart_cubit.dart';
import 'package:socialization/features/charts/models/chart_models.dart';

class ChartTimeSumm extends StatelessWidget {
  final attempId;
  const ChartTimeSumm({Key key, @required this.attempId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height / 2.5;
    return BlocProvider(
      create: (context) =>
          ChartCubit(context.read<ChartsRepository>(), ChartType.timeSumm),
      child: BlocBuilder<ChartCubit, ChartState>(
        builder: (context, state) {
          if (state is ChartInitial) {
            context.read<ChartCubit>().getChartData(attempId);
          }
          if (state is ChartLoaded) {
            return Column(
              children: [
                Text(
                  'Игрвое время',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Container(
                    height: height,
                    child: StackedFillColorBarChart(state.chartData)),
              ],
            );
          }
          return Container();
        },
      ),
    );
  }
}

class StackedFillColorBarChart extends StatelessWidget {
  final List<ChartTimeSummData> chartData;
  final bool animate;

  StackedFillColorBarChart(
    this.chartData, {
    this.animate,
  });
  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      _createSampleData(),
      animate: animate,
      defaultRenderer: new charts.BarRendererConfig(
          groupingType: charts.BarGroupingType.stacked, strokeWidthPx: 2.0),
    );
  }

  /// Create series list with multiple series
  List<charts.Series<ChartTimeSummData, String>> _createSampleData() {
    final desktopSalesData = chartData;
    return [
      new charts.Series<ChartTimeSummData, String>(
        id: 'Advantages',
        domainFn: (ChartTimeSummData sales, _) => sales.lvlName,
        measureFn: (ChartTimeSummData sales, _) => sales.spendTime,
        data: desktopSalesData,
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        fillColorFn: (_, __) =>
            charts.MaterialPalette.blue.shadeDefault.lighter,
      ),
    ];
  }
}

// class HorizontalBarChart extends StatelessWidget {
//   final List<ChartTimeSummData> chartData;
//   final bool animate;

//   HorizontalBarChart(this.chartData, {this.animate});

//   @override
//   Widget build(BuildContext context) {
//     // For horizontal bar charts, set the [vertical] flag to false.
//     return new charts.BarChart(
//       _createSampleData(),
//       animate: animate,
//       vertical: false,
//     );
//   }

//   /// Create one series with sample hard coded data.
//   List<charts.Series<ChartTimeSummData, String>> _createSampleData() {
//     final data = chartData;

//     return [
//       new charts.Series<ChartTimeSummData, String>(
//         id: 'Sales',
//         domainFn: (ChartTimeSummData sales, _) => sales.lvlName,
//         measureFn: (ChartTimeSummData sales, _) => sales.spendTime,
//         data: data,
//       )
//     ];
//   }
// }

