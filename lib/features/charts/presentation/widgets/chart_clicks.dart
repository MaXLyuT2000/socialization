import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/charts/api/charts_repository.dart';
import 'package:socialization/features/charts/logic/chart/chart_cubit.dart';
import 'package:socialization/features/charts/models/chart_models.dart';

class ChartClicks extends StatelessWidget {
  final attempId;
  const ChartClicks({Key key, @required this.attempId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height / 2.5;
    return BlocProvider(
      create: (context) =>
          ChartCubit(context.read<ChartsRepository>(), ChartType.clicks),
      child: BlocBuilder<ChartCubit, ChartState>(
        builder: (context, state) {
          if (state is ChartInitial) {
            context.read<ChartCubit>().getChartData(attempId);
          }
          if (state is ChartLoaded) {
            return Column(
              children: [
                Text(
                  'Статистика кликов',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Container(
                    height: height,
                    child: StackedFillColorBarChart(state.chartData)),
              ],
            );
          }
          return Container();
        },
      ),
    );
  }
}

class StackedFillColorBarChart extends StatelessWidget {
  final List<ChartClicksData> chartData;
  final bool animate;

  StackedFillColorBarChart(
    this.chartData, {
    this.animate,
  });
  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      _createSampleData(),
      animate: animate,
      defaultRenderer: new charts.BarRendererConfig(
          groupingType: charts.BarGroupingType.stacked, strokeWidthPx: 2.0),
    );
  }

  /// Create series list with multiple series
  List<charts.Series<ChartClicksData, String>> _createSampleData() {
    final desktopSalesData = chartData;
    return [
      new charts.Series<ChartClicksData, String>(
        id: 'Advantages',
        domainFn: (ChartClicksData sales, _) => sales.lvlName,
        measureFn: (ChartClicksData sales, _) => sales.missclicks,
        data: desktopSalesData,
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        fillColorFn: (_, __) =>
            charts.MaterialPalette.blue.shadeDefault.lighter,
      ),
      new charts.Series<ChartClicksData, String>(
        id: 'Disadvantages',
        measureFn: (ChartClicksData sales, _) => sales.clicks,
        data: desktopSalesData,
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (ChartClicksData sales, _) => sales.lvlName,
      ),
    ];
  }
}
