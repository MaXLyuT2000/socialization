import 'package:flutter/material.dart';
import 'package:socialization/features/charts/presentation/widgets/chart_answers.dart';
import 'package:socialization/features/charts/presentation/widgets/chart_clicks.dart';
import 'package:socialization/features/charts/presentation/widgets/chart_time_summ.dart';

class ChartsScreen extends StatelessWidget {
  final String attempId;
  const ChartsScreen({
    Key key,
    this.attempId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Статистика',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(children: [
            ChartTimeSumm(attempId: attempId),
            ChartClicks(
              attempId: attempId,
            ),
            ChartAnswers(
              attempId: attempId,
            ),
          ]),
        ),
      ),
    );
  }
}
