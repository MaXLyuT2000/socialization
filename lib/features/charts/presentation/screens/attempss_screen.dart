import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/charts/api/charts_repository.dart';
import 'package:socialization/features/charts/presentation/screens/charts_scree.dart';
import 'package:socialization/features/charts/logic/attemps/attemps_cubit.dart';

class AttempsScreen extends StatelessWidget {
  // final String gameId = '59cbf38d-b808-4353-bafc-3c5889993bb5';
  final String gameId;
  const AttempsScreen({
    Key key,
    this.gameId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final attempsCubit = AttempsCubit(context.read<ChartsRepository>());
    final width = MediaQuery.of(context).size.width;
    final buttonStyle = ButtonStyle(
      elevation: MaterialStateProperty.all<double>(10),
      shadowColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
      minimumSize: MaterialStateProperty.all<Size>(Size(width - 100, 50)),
    );
    return BlocProvider(
      create: (context) => attempsCubit,
      child: Scaffold(
        appBar: AppBar(
            title:
                Text('Список попыток', style: TextStyle(color: Colors.black))),
        body: BlocBuilder<AttempsCubit, AttempsState>(
          builder: (context, state) {
            if (state is AttempsInitial) {
              context.read<AttempsCubit>().getAttemps(gameId);
            }
            if (state is AttempsLoaded) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  state.attemps.isEmpty
                      ? Center(
                          child: Text(
                            'Нет статистики',
                          ),
                        )
                      : ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: state.attemps.length,
                          itemBuilder: ((context, index) {
                            return Padding(
                              padding: EdgeInsets.all(20),
                              child: ElevatedButton(
                                  style: buttonStyle,
                                  onPressed: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return ChartsScreen(
                                        attempId: state.attemps[index],
                                      );
                                    }));
                                  },
                                  child: Text('Попытка № ${index + 1}')),
                            );
                          }))
                ],
              );
            }
            return Text('fail');
          },
        ),
      ),
    );
  }
}
