import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:dio/dio.dart';
import 'package:socialization/common/app_settings.dart';
import 'package:socialization/features/authorization/models/profile.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

enum Roles { Patient, Tutor }

class UserRepository {
  static const String mainUrl = AppSettengs.apiUrl;
  static const String tutorAccountUrl = '$mainUrl/tutor/account';
  static const String patientAccountUrl = '$mainUrl/patient/account';
  static const String tutorImageAdd = '$mainUrl/tutor/account/image/add';
  static const String patientImageAdd = '$mainUrl/patient/account/image/add';
  static const loginUrl = '$mainUrl/users/authorization';

  Roles role;
  String token;
  User profile;

  final FlutterSecureStorage storage = new FlutterSecureStorage();
  final Dio _dio = Dio();

  Future<bool> hasToken() async {
    var value = await storage.read(key: 'token');
    if (value != null) {
      await storage
          .read(key: 'role')
          .then((value) => role = _decodeRole(value));
      token = value;
      return true;
    } else {
      return false;
    }
  }

  Roles _decodeRole(String role) {
    switch (role) {
      case 'Tutor':
        return Roles.Tutor;
        break;
      case 'Patient':
        return Roles.Patient;
        break;
      default:
    }
  }

  Future<void> persistToken(String token) async {
    await storage.write(key: 'token', value: token);
    await storage.write(key: 'role', value: role.name);
  }

  Future<void> deleteToken() async {
    storage.delete(key: 'token');
    storage.delete(key: 'role');
    profile = null;
    storage.deleteAll();
  }

  Future<String> login(String login, String password) async {
    Response response = await _dio.post(loginUrl, data: {
      "credentials": login,
      "password": password,
    });
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      token = jsonObject["token"];
      switch (jsonObject["role"]) {
        case 3:
          role = Roles.Tutor;
          break;
        case 1:
          role = Roles.Patient;
          break;
        default:
          throw Exception();
      }

      if (token == null) throw Exception(jsonObject["message"]);
      return token;
    }
  }

  //Получаение полнйо информации о профиле
  Future<void> getProfileInfo() async {
    String profileUrl = '';
    switch (role) {
      case Roles.Tutor:
        profileUrl = tutorAccountUrl;
        break;
      case Roles.Patient:
        profileUrl = patientAccountUrl;
        break;
      default:
    }

    Response response =
        await _dio.get(profileUrl, options: Options(headers: {"token": token}));
    if (response.statusCode == 200) {
      var jsonObject = response.data;
      profile = User.fromJson(jsonObject);
    }
  }

  //Загрузка изображения профиля пользователя
  Future<String> postProfileImage(File file) async {
    String url;
    switch (role) {
      case Roles.Tutor:
        url = tutorImageAdd;
        break;
      case Roles.Patient:
        url = patientImageAdd;
        break;
      default:
    }
    var request = http.MultipartRequest("POST", Uri.parse(url));
    request.headers['token'] = token;

    var image = await http.MultipartFile.fromPath('image', file.path);
    request.files.add(image);
    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var result = String.fromCharCodes(responseData);
    final String loadedImage = AppSettengs.imgUrl + jsonDecode(result)['image'];
    profile.image = loadedImage;
    return loadedImage;
  }
}
