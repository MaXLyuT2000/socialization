import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';

import 'auth_event.dart';
import 'auth_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;

  AuthenticationBloc({@required this.userRepository})
      : super(AuthenticationUninitialized()) {
    on<AppStarted>(_onAppStarted);
    on<LoggedIn>(_onLoggedIn);
    on<LoggedOut>(_onLoggedOut);
  }

  //При запуске приложения получаем инофрмацию о профиле
  void _onAppStarted(event, Emitter<AuthenticationState> emit) async {
    if (event is AppStarted) {
      final bool hasToken = await userRepository.hasToken();
      if (hasToken) {
        await userRepository.getProfileInfo();
        emit(AuthenticationAuthenticated());
      } else {
        emit(AuthenticationUnauthenticated());
      }
    }
  }

  void _onLoggedIn(event, Emitter<AuthenticationState> emit) async {
    if (event is LoggedIn) {
      emit(AuthenticationLoading());
      await userRepository.persistToken(event.token);
      await userRepository.getProfileInfo();
      emit(AuthenticationAuthenticated());
    }
  }

  void _onLoggedOut(event, Emitter<AuthenticationState> emit) async {
    if (event is LoggedOut) {
      emit(AuthenticationLoading());
      await userRepository.deleteToken();
      emit(AuthenticationUnauthenticated());
    }
  }
}
