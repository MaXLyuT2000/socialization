import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/authorization/presentation/auth_bloc/auth_bloc.dart';
import 'package:socialization/features/authorization/presentation/auth_bloc/auth_event.dart';

import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  }) : super(LoginInitial()) {
    on<LoginButtonPressed>(_onLoginButtonPressed);
  }

  //При нажатии кнопки "Авторизироваться"
  void _onLoginButtonPressed(event, Emitter<LoginState> emit) async {
    if (event is LoginButtonPressed) {
      emit(LoginLoading());
      try {
        final token = await userRepository.login(
          event.login,
          event.password,
        );
        authenticationBloc.add(LoggedIn(token: token));
        emit(LoginInitial());
      } catch (error) {
        emit(LoginFailure(error: error.toString()));
      }
    }
  }
}
