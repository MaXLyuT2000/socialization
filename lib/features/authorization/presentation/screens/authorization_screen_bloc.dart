import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/common/app_styles.dart';
import 'package:socialization/features/authorization/api/authorization_repository.dart';
import 'package:socialization/features/authorization/presentation/auth_bloc/auth_bloc.dart';
import 'package:socialization/features/authorization/presentation/auth_bloc/auth_state.dart';
import 'package:socialization/features/authorization/presentation/screens/login_screen.dart';
import 'package:socialization/features/main/presentation/screens/bot_nav_bar.dart';
import 'package:socialization/features/profile/presentation/cubit/profile_avatar_dart_cubit.dart';

class AuthorizationScreenBloc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (context, state) {
        if (state is AuthenticationAuthenticated) {
          return MultiBlocProvider(providers: [
            BlocProvider<ProfileAvatarDartCubit>(
                create: (context) => ProfileAvatarDartCubit(
                    repository: context.read<UserRepository>(),
                    image: context.read<UserRepository>().profile.image))
          ], child: BotNavBar());
        }
        if (state is AuthenticationUnauthenticated) {
          // return IntroPage();
          return LoginScreen(
            userRepository: context.read<UserRepository>(),
          );
        }
        if (state is AuthenticationLoading) {
          return Loadingindicator();
        }
        return Loadingindicator();
      },
    );
  }
}

class Loadingindicator extends StatelessWidget {
  const Loadingindicator({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 25.0,
              width: 25.0,
              child: CircularProgressIndicator(
                valueColor:
                    new AlwaysStoppedAnimation<Color>(AppStyles.mainColor),
                strokeWidth: 4.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
