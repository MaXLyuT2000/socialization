import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/features/authorization/presentation/auth_bloc/auth_bloc.dart';
import 'package:socialization/features/authorization/presentation/auth_bloc/auth_event.dart';

showAlertErrorDialog(BuildContext context) {
  // set up the button
  Widget yesButton = TextButton(
    child: Text("Да"),
    onPressed: () {
      BlocProvider.of<AuthenticationBloc>(context).add(
        LoggedOut(),
      );
      Navigator.pop(context);
    },
  );

  Widget noButton = TextButton(
    child: Text("Нет"),
    onPressed: () {
      Navigator.pop(context);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Container(
      child: Text("Предупреждение"),
      alignment: Alignment.center,
    ),
    content: Container(
      child: Text(
        "Вы действительно хотите выйти?",
        softWrap: true,
      ),
    ),
    actions: [yesButton, noButton],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
