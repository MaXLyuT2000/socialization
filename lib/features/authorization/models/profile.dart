import 'package:flutter/foundation.dart';

class User {
  final String id;
  final String name;
  final String surname;
  final String secondName;
  String image;
  final String organization;
  final String email;
  final String gender;
  final String numberPhone;
  final String birthDate;

  User({
    @required this.secondName,
    @required this.email,
    @required this.id,
    @required this.name,
    @required this.surname,
    @required this.image,
    @required this.organization,
    @required this.gender,
    @required this.numberPhone,
    @required this.birthDate,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    final List<dynamic> gamesList = json['games'];
    return User(
      id: json['id'] ?? null,
      name: json['name'] ?? null,
      surname: json['surname'] ?? null,
      secondName: json['secondName'] ?? null,
      email: json['email'] ?? 'Неизвестно',
      image: json['image'] == null
          ? null
          : 'https://mobile.itkostroma.ru/images/' + json['image'],
      organization: json['organizationString'] ?? 'Неизвестно',
      gender: json['gender'] ?? 'Неизвестно',
      numberPhone: json['numberPhone'] ?? 'Неизвестно',
      birthDate: json['birthDate'] ?? 'Неизвестно',
    );
  }
}

var tutorProf = {
  "id": "16a15674-e976-437b-8f63-2f3c3ec524e1",
  "name": "Мартин",
  "surname": "Лютер",
  "secondName": "Кинг",
  "image":
      "https://mobile.itkostroma.ru/images/1acb28f9-1e43-4e06-bdf1-9c3f52ab532a.jpg",
  "organizationString": "TestOrganization",
  "organization": "c813a5d2-a851-4194-9b12-5c26ac7b2417",
  "login": "tutor",
  "email": "tutor@mail.ru",
};

var patientProf = {
  "id": "1acb28f9-1e43-4e06-bdf1-9c3f52ab532a",
  "name": "Элайджа",
  "surname": "Вуд",
  "secondName": "Митеда",
  "image":
      "https://mobile.itkostroma.ru/images/1acb28f9-1e43-4e06-bdf1-9c3f52ab532a.jpg",
  "organizationString": "TestOrganization",
  "organization": "c813a5d2-a851-4194-9b12-5c26ac7b2417",
  "login": "patient",
  "email": "patient@mail.ru",
  "address": null,
  "gender": "Мужской",
  "numberPhone": null,
  "birthDate": "2022-05-03 00:00:00.0",
};

var patientByID = {
  "id": "1acb28f9-1e43-4e06-bdf1-9c3f52ab532a",
  "name": "Элайджа",
  "surname": "Вуд",
  "secondName": "Митеда",
  "image":
      "https://mobile.itkostroma.ru/images/1acb28f9-1e43-4e06-bdf1-9c3f52ab532a.jpg",
  "organization": "TestOrganization",
  "login": "patient",
  "email": "patient@mail.ru",
  "address": null,
  "gender": "Мужской",
  "numberPhone": null,
  "birthDate": "2022-05-03T00:00:00.000+00:00",
  "tutor": null,
  "games": [],
};
