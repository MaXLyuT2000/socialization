import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialization/common/app_styles.dart';
import 'package:socialization/features/charts/api/charts_repository.dart';
import 'package:socialization/features/charts/presentation/screens/attempss_screen.dart';
import 'package:socialization/features/games/api/games_repository.dart';
import 'package:socialization/features/games/presentation/games/games_cubit.dart';
import 'package:socialization/features/games/presentation/screens/games_cubit.dart';
import 'package:socialization/features/patients/api/patient_repository.dart';
import 'package:socialization/features/patients/presentation/patients/patients_cubit.dart';
import 'package:socialization/features/patients/presentation/screens/patients_add/patients_add_cubit.dart';
import 'package:socialization/features/patients/presentation/screens/patients_list/patients_screen.dart';
import 'package:socialization/features/patients_profile/presentation/screens/patient_info_cubit.dart';
import 'features/authorization/api/authorization_repository.dart';
import 'features/authorization/presentation/auth_bloc/auth_bloc.dart';
import 'features/authorization/presentation/auth_bloc/auth_event.dart';
import 'features/authorization/presentation/screens/authorization_screen_bloc.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() async {
  HttpOverrides.global = new MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  // final themeServise = await ThemeService.instance;
  // var initTheme = themeServise.initial;
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(
    MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (_) => UserRepository()),
        RepositoryProvider(create: (_) => GamesRepository()),
        RepositoryProvider(
          create: (_) => PatientsRepository(),
        ),
        RepositoryProvider(create: (_) => ChartsRepository())
      ],
      child: MultiBlocProvider(
          providers: [
            BlocProvider<AuthenticationBloc>(
              create: (context) {
                return AuthenticationBloc(
                    userRepository: context.read<UserRepository>())
                  ..add(AppStarted());
              },
            ),
            BlocProvider<GamesCubit>(create: (context) {
              return GamesCubit(
                  repository: context.read<GamesRepository>(),
                  profile: context.read<UserRepository>());
            }),
            BlocProvider<PatientsCubit>(
                create: (context) => PatientsCubit(
                    token: context.read<UserRepository>().token,
                    repository: context.read<PatientsRepository>())),
          ],
          child: MaterialApp(
            routes: {
              '/Observers': (BuildContext context) => PatientsScreen(),
              '/Authorization': (BuildContext context) =>
                  AuthorizationScreenBloc(),
              '/PatientInfo': (BuildContext context) =>
                  PatientInfoScreenCubit(),
              '/PatientsAdd': (BuildContext context) => PatientsAddListCubit(),
              '/AllGames': (BuildContext context) => GamesListCubit(),
              '/Test': (BuildContext context) => AttempsScreen(),
            },
            initialRoute: '/Authorization',
            debugShowCheckedModeBanner: false,
            // locale: const Locale('mn', 'MN'),
            theme: AppStyles.lightTheme,
            // darkTheme: AppStyles.darkTheme,
          )),
    ),
  );
}
